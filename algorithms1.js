'use strict';

console.log('Example CLI Input: ' + process.argv[1]);
/* ************************************************************************************************
 * Problem: Create a function that returns whether or not a given growth rate table is valid
 * 
 * A table is valid if:
 *   It spans all weeks 1 through 53.
 *   Each row can have multiple weeks. 
 *   All rows cover a time period moving forward
 *   No overlapping weeks appear in the table. 
 *   The sum of all growth percentages in the table is equal to 1
 * 
 * Example Valid Table:
 *   Row Index   StartWeek   EndWeek     GrowthPct
 *   1           1           4           .1
 *   2           8           10          .17
 *   3           5           7           .05
 *   4           11          53          .68
 * 
 * Other notes:
 *   The table does not have a guaranteed order
 *   The order of priorities for the solution should be:
 *     - Correctness
 *     - Elegance/Style
 *     - Performance
*/

/**
 * Validates a GrowthTable object.
 * @param {GrowthTable} growthTable
 * @returns {boolean} 
 */
function isValid(growthTable) {
  // add your code here
}

class GrowthTable {
  constructor(rows) {
    this._rows = rows;
  }
  
  get rows() {
    return this._rows;
  }
}

class GrowthTableRow {
  constructor(startWeek, endWeek, growthPct) {
    this._startWeek = startWeek;
    this._endWeek = endWeek;
    this._growthPct = growthPct;
  }

  get startWeek() {
    return this._startWeek;
  }
  
  get endWeek() {
    return this._endWeek;
  }

  get growthPct() {
    return this._growthPct;
  }
}

module.exports = isValid;
