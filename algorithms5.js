'use strict';


// Record should be indexed by the combination of pk_1 and pk_2 (each are part of the "primary key"). 
// loadRecordsIntoCache should replace any element in the cache with identical pk_1 and pk_2 combinations
// The order of priorities for the solution should be: Correctness, Performance, and Style.

/**
 * Returns a cache of records.
 * @param {Array<Record>} records 
 */
function loadRecordsIntoCache(records) {
  // your code goes here
}

// Create an efficient cache of records. Use your own data structure, rather than a caching package

/**
 * Returns a record from the cache
 * @param {Number} pk1 
 * @param {Number} pk2
 * @returns {Record}
 */
function getRecord(pk1, pk2) {
  // your code goes here
}

class Record {
  constructor(pk1, pk2, value) {
    this._pk1 = pk1;
    this._pk2 = pk2;
    this._value = value;
  }

  get pk1() {
    return this._pk1;
  }

  get pk2() {
    return this._pk2;
  }

  get value() {
    return this._value;
  }
}