'use strict';

const sampleData = [
  new Person('Amy', 'Monroe', 54),
  new Person('Amy', 'Duval', 54),
  new Person('Joe', 'Conrad', 14),
  new Person('Amy', 'Jenkins', 34),
  new Person('Bill', 'Monroe', 34),
  new Person('Amy', 'Smith', 34),
  new Person('Joe', 'Johnson', 14),
]

/**
 * Returns a list of people by last name and then by first name.
 * 
 * @param {Array<Person>} people
 * @returns {Array<String>}
 */
function sortByLastNameThenFirstName(people) {
  // Write code to return a list of strings sorted by last name, 
  // then first name, then age. Use the format: `${LastName} - ${FirstName} - ${Age}`
  // Example string: Burgundy - Ron - 43
  return [];
}

/**
 * 
 * @param {Array<Person>} people 
 * @returns {Array<String>}
 */
function countsByFirstName(people) {
  // Write code to return a list of strings string with a count of the number of times each first name occurs, ordered by occurence.
  // Use this format: `${name}, ${count}`
  // When printed the output should look like this:
  // Example string: Ron, 2
  return [];
}

class Person {
  constructor(firstName, lastName, age) {
    this._firstName = firstName;
    this._lastName = lastName;
    this._age = age;
  }

  get firstName() {
    return this._firstName;
  }
  
  get lastName() {
    return this._lastName;
  }

  get age() {
    return this._age;
  }
}