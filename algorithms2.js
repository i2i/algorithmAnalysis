'use strict';

/* ************************************************************************************************
 * Problem: Implement the batch function.
 * 
 * I would like to return a stream of batches of size <= batchCount while enumerating over the
 * input items. Each batch is defined as an array where the number of elements in the array <= batchCount
 */

/**
 * Generates a list of items in batches.
 * @param {items} an array of objects
 * @param {batchSize} batchSize The batch size
 * @returns  {object[]}
 */
function* batch(items, batchSize) {
  yield undefined;
}

module.exports = batch;
